Welcome to django risk free rate's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   testing
   documentation
   markdown.md
   django_apps/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
