Documentation
=============

Tools
-----
The documentation is built with ``sphinx``.

Configuration
-------------
The configuration is stored in ``conf.py``

Building
--------
The markup can be rendered to static html using:

.. code-block::

   # docs/Makefile
   $ make html

This should work both in the ``docs`` directory and the
project root directory since the docs ``Makefile`` is connected to
the general project ``Makefile``.

Doc Strings
-----------
The google style is preferred for docstrings.
They are handled by the sphinx napoleon extension.


Additional Resources
--------------------

**links-to-external-web-pages**

- https://sublime-and-sphinx-guide.readthedocs.io/en/latest/references.html#links-to-external-web-pages

**RST reference**

- https://docutils.sourceforge.io/docs/user/rst/quickref.html

- https://docutils.sourceforge.io/rst.html

**Napoleon config options**
 - https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html#module-sphinx.ext.napoleon

**Theme Config options**
 - https://sphinx-rtd-theme.readthedocs.io

**Autodoc options**
 - https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html

**hot rebuilding the docs on change**
- https://pypi.org/project/sphinx-autobuild/
