Testing
=======


Testing Setup
-------------

``pytest`` is used as the testrunner. Test can be written
in both unittest style or pytest style. The unittest style is preferred.
pytest style should be used where it is easier.

test configuration
^^^^^^^^^^^^^^^^^^

Some configuration is stored inside ``pytest.ini``.

running the tests
-----------------
Make sure the database is running.

.. code-block::

   $ docker-compose up
   Starting ratesdj2_postgres_1 ...
   Starting ratesdj2_postgres_1 ... done
   Attaching to ratesdj2_postgres_1
   postgres_1  |
   postgres_1  | PostgreSQL Database directory appears to contain a database; Skipping initialization
   postgres_1  |
   postgres_1  | 2020-11-01 17:46:29.031 UTC [1] LOG:  starting PostgreSQL 13beta3 on x86_64-pc-linux-musl, compiled by gcc (Alpine 9.3.0) 9.3.0, 64-bit
   postgres_1  | 2020-11-01 17:46:29.032 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
   postgres_1  | 2020-11-01 17:46:29.034 UTC [1] LOG:  listening on IPv6 address "::", port 5432
   postgres_1  | 2020-11-01 17:46:29.044 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
   postgres_1  | 2020-11-01 17:46:29.069 UTC [20] LOG:  database system was shut down at 2020-10-25 12:27:53 UTC
   postgres_1  | 2020-11-01 17:46:29.083 UTC [1] LOG:  database system is ready to accept connections


The tests can be initiated in the project root directory.

.. code-block::

   $ pytest -v --cov=.

Notice the coverage flag ``--cov=.``

coverage
--------

After a test run with coverage, an HTML report can be created using:
.. code-block::

   $ coverage html && firefox ./htmlcov/index.html

A sample report offers a summary.

.. image:: _static/testing/coverage_report_summary.png
   :alt: coverage offers a summary per python module
   :scale: 50 %

(test) code coverage is specifically useful to detect untested branches:

.. image:: _static/testing/coverage_report_detail.png
   :alt: detect missed branch easily
   :scale: 50 %

A coverage configuration file is currently not in use.


Continuous Integration
----------------------

Possible candidates were ``cirecle CI``, ``travis CI`` and ``gitlab CI``.
Travis was used early on when the project was developed on github.com.
The CI system was migrated to gitlab, after the project was migrated to gitlab.

CI Configuration
^^^^^^^^^^^^^^^^
The current configuration is stored in ``.gitlab-ci.yml``.
https://gitlab.com/erkandem/rates_dj/-/blob/03dd1b89d595b45c3a59311b727461ece37d7b8d/.gitlab-ci.yml
.travis.yml is deprecated.
