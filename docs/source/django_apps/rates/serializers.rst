===========
Serializers
===========


.. autoclass:: rates.serializers.ECBRateSerializerFieldNameValidation
   :members:


.. autoclass:: rates.serializers.ECBRateSerializer
   :members:
   :show-inheritance:

   .. automethod:: __init__
