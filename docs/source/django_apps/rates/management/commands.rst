========
Commands
========


Populating some Data
====================
.. automodule:: rates.management.commands.load_rates
   :members:


Adding new (quality) data
=========================

.. automodule:: rates.management.commands.update_rates_in_database
   :members:
