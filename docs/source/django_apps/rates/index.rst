=========
Rates App
=========


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   models
   serializers
   views
   management/index
